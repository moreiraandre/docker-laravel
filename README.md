# Build image
```bash
docker image build -t moreiraandre/nginx-php .
```

# Criar container
```bash
docker container run -d -p 8080:80 -v "$(pwd)"/sig-laravel:/var/www/html/sig-laravel --name nginx-php moreiraandre/nginx-php
```

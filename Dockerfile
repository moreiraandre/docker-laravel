FROM debian:9
LABEL maintainer="Andre Moreira <andre.mcx1@gmail.com>"

RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install -y ca-certificates apt-transport-https
RUN apt-get install -y git
RUN apt-get install -y nginx
RUN apt-get install -y curl
RUN apt-get install -y wget
RUN apt-get install -y software-properties-common
RUN apt-get install -y gnupg2
RUN wget -q https://packages.sury.org/php/apt.gpg -O- | apt-key add -
RUN echo "deb https://packages.sury.org/php/ stretch main" | tee /etc/apt/sources.list.d/php.list
RUN apt-get update
RUN apt-get install -y php7.2 php7.2-cli php7.2-common php7.2-mbstring php7.2-gd php7.2-intl php7.2-xml php7.2-mysql php7.2-zip php7.2-fpm php7.2-bcmath php7.2-curl php7.2-pgsql php7.2-json php7.2-ldap
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
ENV COMPOSER_ALLOW_SUPERUSER 1
#RUN chown -R $USER ~/.composer/
RUN echo 'export PATH="$HOME/.composer/vendor/bin:$PATH"' >> ~/.bashrc
RUN . ~/.bashrc

RUN ln -sf /dev/stdout /var/log/nginx/access.log
RUN ln -sf /dev/stderr /var/log/nginx/error.log
RUN mkdir /var/www/html/app
VOLUME ["/var/cache/nginx", "/var/www/html/app"]

RUN rm /etc/nginx/sites-available/default
ADD ./default /etc/nginx/sites-available/default

WORKDIR /var/www/html

EXPOSE 80 443

CMD service php7.2-fpm start && nginx -g "daemon off;"
